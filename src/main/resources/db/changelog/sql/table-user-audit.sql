CREATE TABLE user_audit
(
    id                      BIGSERIAL PRIMARY KEY,
    operation               VARCHAR(10)  NOT NULL,
    created_date            TIMESTAMP,
    last_modified_date      TIMESTAMP,
    user_id                 BIGINT       NOT NULL,
    current_firstname       VARCHAR(50),
    old_firstname           VARCHAR(50)  NOT NULL,
    current_lastname        VARCHAR(50),
    old_lastname            VARCHAR(50)  NOT NULL,
    current_email           VARCHAR(50),
    old_email               VARCHAR(50)  NOT NULL,
    current_login           VARCHAR(50),
    old_login               VARCHAR(50)  NOT NULL,
    current_password        VARCHAR(255),
    old_password            VARCHAR(255) NOT NULL,
    current_start_work_hour TIMESTAMP,
    old_start_work_hour     TIMESTAMP    NOT NULL,
    current_end_work_hour   TIMESTAMP,
    old_end_work_hour       TIMESTAMP    NOT NULL,
    current_role_id         BIGINT,
    old_role_id             BIGINT       NOT NULL

);