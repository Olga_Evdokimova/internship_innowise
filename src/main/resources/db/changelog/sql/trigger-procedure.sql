CREATE
OR REPLACE FUNCTION user_audit_insert() RETURNS TRIGGER AS
$$
BEGIN

INSERT INTO user_audit(operation, created_date, last_modified_date, user_id, current_firstname, old_firstname,
                       current_lastname, old_lastname, current_email, old_email, current_login, old_login,
                       current_password, old_password, current_start_work_hour, old_start_work_hour,
                       current_end_work_hour, old_end_work_hour, current_role_id, old_role_id)
VALUES ('INSERT', now(), now(), NEW.id, NEW.firstname, NEW.firstname, NEW.lastname,
        NEW.lastname,
        NEW.email, NEW.email, NEW.login, NEW.login, NEW.password, NEW.password, NEW.start_work_hour,
        NEW.start_work_hour,
        NEW.end_work_hour, NEW.end_work_hour, NEW.role_id, NEW.role_id);
RETURN NEW;

END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER user_audit_insert
    AFTER INSERT
    ON user_detail
    FOR EACH ROW
    EXECUTE PROCEDURE user_audit_insert();

CREATE
OR REPLACE FUNCTION user_audit_update() RETURNS TRIGGER AS
$$
BEGIN

INSERT INTO user_audit(operation, last_modified_date,
                       user_id, current_firstname, old_firstname,
                       current_lastname, old_lastname, current_email, old_email, current_login, old_login,
                       current_password, old_password, current_start_work_hour, old_start_work_hour,
                       current_end_work_hour, old_end_work_hour, current_role_id, old_role_id)
VALUES ('UPDATE',
        now(),
        (SELECT NEW.id),
        (SELECT NEW.firstname),
        (SELECT OLD.firstname),
        (SELECT NEW.lastname),
        (SELECT OLD.lastname),
        (SELECT NEW.email),
        (SELECT OLD.email),
        (SELECT NEW.login),
        (SELECT OLD.login),
        (SELECT NEW.password),
        (SELECT OLD.password),
        (SELECT NEW.start_work_hour),
        (SELECT OLD.start_work_hour),
        (SELECT NEW.end_work_hour),
        (SELECT OLD.end_work_hour),
        (SELECT NEW.role_id),
        (SELECT OLD.role_id));
RETURN NEW;

END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER user_audit_update
    AFTER UPDATE
    ON user_detail
    FOR EACH ROW
    EXECUTE PROCEDURE user_audit_update();


CREATE
OR REPLACE FUNCTION user_audit_delete() RETURNS TRIGGER AS
$$
BEGIN

INSERT INTO user_audit(operation, last_modified_date,
                       user_id, old_firstname,
                       old_lastname, old_email, old_login,
                       old_password, old_start_work_hour,
                       old_end_work_hour, old_role_id)
VALUES ('DELETE',
        now(),
        (SELECT OLD.id),
        (SELECT OLD.firstname),
        (SELECT OLD.lastname),
        (SELECT OLD.email),
        (SELECT OLD.login),
        (SELECT OLD.password),
        (SELECT OLD.start_work_hour),
        (SELECT OLD.end_work_hour),
        (SELECT OLD.role_id));
RETURN OLD;

END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER user_audit_delete
    AFTER DELETE
    ON user_detail
    FOR EACH ROW
    EXECUTE PROCEDURE user_audit_delete();
