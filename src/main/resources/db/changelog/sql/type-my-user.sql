CREATE TYPE my_user as
    (
    id BIGINT,
    operation VARCHAR(10),
    created_date TIMESTAMP,
    last_modified_date TIMESTAMP,
    user_id BIGINT,
    current_firstname VARCHAR(50),
    old_firstname VARCHAR(50),
    current_lastname VARCHAR(50),
    old_lastname VARCHAR(50),
    current_email VARCHAR(50),
    old_email VARCHAR(50),
    current_login VARCHAR(50),
    old_login VARCHAR(50),
    current_password VARCHAR(255),
    old_password VARCHAR(255),
    current_start_work_hour TIMESTAMP,
    old_start_work_hour TIMESTAMP,
    current_end_work_hour TIMESTAMP,
    old_end_work_hour TIMESTAMP,
    current_role_id BIGINT,
    old_role_id BIGINT

    );