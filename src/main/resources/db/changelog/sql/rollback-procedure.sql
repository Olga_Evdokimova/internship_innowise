CREATE PROCEDURE rollback_user(date_for_rollback TIMESTAMP) AS
$$
DECLARE
sel my_user;
    curs_for_dates
cursor (date_for_rollback TIMESTAMP)
        FOR
SELECT id,
       operation,
       created_date,
       last_modified_date,
       user_id,
       current_firstname,
       old_firstname,
       current_lastname,
       old_lastname,
       current_email,
       old_email,
       current_login,
       old_login,
       current_password,
       old_password,
       current_start_work_hour,
       old_start_work_hour,
       current_end_work_hour,
       old_end_work_hour,
       current_role_id,
       old_role_id
FROM user_audit
WHERE last_modified_date >= date_for_rollback
ORDER BY last_modified_date desc;
BEGIN
OPEN curs_for_dates(date_for_rollback);

LOOP
FETCH curs_for_dates INTO sel;
        EXIT
WHEN NOT found;
CASE (sel.operation)
            WHEN 'DELETE' THEN execute func_rollback_for_delete(sel);
WHEN 'UPDATE' THEN execute func_rollback_for_update(sel);
WHEN 'INSERT' THEN execute func_rollback_for_insert(sel);
END
CASE;
END LOOP;

CLOSE curs_for_dates;
END;
$$
LANGUAGE plpgsql;



CREATE FUNCTION func_rollback_for_delete(sel my_user) RETURNS VOID AS
    $$
BEGIN
INSERT INTO user_detail(id, firstname, lastname, email, login, password, start_work_hour,
                        end_work_hour, role_id)
VALUES (sel.user_id, sel.old_firstname, sel.old_lastname, sel.old_email, sel.old_login,
        sel.old_password,
        sel.old_start_work_hour, sel.old_end_work_hour, sel.old_role_id);
END;
$$
LANGUAGE plpgsql;

CREATE FUNCTION func_rollback_for_update(sel my_user) RETURNS VOID AS
    $$
BEGIN
UPDATE user_detail
SET firstname       = sel.old_firstname,
    lastname        = sel.old_lastname,
    email           = sel.old_email,
    login           = sel.old_login,
    password        = sel.old_password,
    start_work_hour = sel.old_start_work_hour,
    end_work_hour   = sel.old_end_work_hour,
    role_id         = sel.old_role_id
WHERE id = sel.user_id;
END;
$$
LANGUAGE plpgsql;

CREATE FUNCTION func_rollback_for_insert(sel my_user) RETURNS VOID AS
    $$
BEGIN
DELETE
FROM refresh_token
WHERE user_id = sel.user_id;
DELETE
FROM user_detail
WHERE id = sel.user_id;
END;
$$
LANGUAGE plpgsql;
