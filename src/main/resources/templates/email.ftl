<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Spring boot send mail example</title>
</head>
<body>
<h3>${name} is late! Login at <span style="color: darkred; ">${currentTime}</span> .
    Should start at <span style="color: midnightblue; ">${startWorkHour}</span></h3>
</body>
</html>