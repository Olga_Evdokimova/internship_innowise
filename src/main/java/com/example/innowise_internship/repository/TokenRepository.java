package com.example.innowise_internship.repository;

import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TokenRepository extends JpaRepository<TokenDetails, Long> {

    Optional<TokenDetails> findByRefreshToken(String refresh);

}
