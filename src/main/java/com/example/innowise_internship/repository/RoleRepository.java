package com.example.innowise_internship.repository;

import com.example.innowise_internship.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
