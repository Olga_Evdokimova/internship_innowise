package com.example.innowise_internship.repository;

import com.example.innowise_internship.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @EntityGraph(attributePaths = {"userRole", "admins"})
    Optional<User> findById(Long id);

    @EntityGraph(attributePaths = {"userRole", "admins"})
    Optional<User> findByEmail(String email);

    @Query("SELECT u.admins FROM User u WHERE u.id = :userId")
    Optional<List<User>> findAllAById(Long userId);

    @Modifying
    @Procedure("rollback_user")
    void rollback(@Param("date") LocalDateTime date);

}
