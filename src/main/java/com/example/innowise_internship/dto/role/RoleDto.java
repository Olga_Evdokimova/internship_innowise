package com.example.innowise_internship.dto.role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(setterPrefix = "with", toBuilder = true)
public class RoleDto {

    private Long id;
    private String name;

}
