package com.example.innowise_internship.dto.user;

import com.example.innowise_internship.dto.role.RoleDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(setterPrefix = "with", toBuilder = true)
public class UserDto {

    private Long id;
    private String firstName;
    private String lastName;
    private RoleDto roleDto;

}
