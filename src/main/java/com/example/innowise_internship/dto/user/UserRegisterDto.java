package com.example.innowise_internship.dto.user;

import com.example.innowise_internship.dto.role.RoleDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder(setterPrefix = "with", toBuilder = true)
public class UserRegisterDto {
    private String firstName;
    private String lastName;
    private RoleDto roleDto;
    private String login;
    private String password;
    private String email;
    private LocalDateTime startWorkHour;
    private LocalDateTime endWorkHour;

}
