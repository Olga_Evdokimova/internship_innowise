package com.example.innowise_internship.utils;

import com.example.innowise_internship.entity.User;

import java.time.LocalDateTime;
import java.util.Map;

public class TemplateUtil {

    public static Map<String, String> prepareTemplateInfo(User user) {
        return Map.of(
                "name", user.getFirstName(),
                "currentTime", DateUtil.formatter(LocalDateTime.now()),
                "startWorkHour", DateUtil.formatter(user.getStartWorkHour())
        );
    }
}
