package com.example.innowise_internship.provider;

import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.JwtTokenPair;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.properties.JwtTokenProperties;
import com.example.innowise_internship.transformer.TokenTransformer;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class TokenHandler {

    private final JwtTokenProperties jwtTokenProperties;
    private final TokenTransformer tokenTransformer;

    public TokenHandler(JwtTokenProperties jwtTokenProperties, TokenTransformer tokenTransformer) {
        this.jwtTokenProperties = jwtTokenProperties;
        this.tokenTransformer = tokenTransformer;
    }

    public String generateAccessToken(User user) {
        return generateToken(user, jwtTokenProperties.getJwtAccessExpiration());
    }

    public String generateRefreshToken(User user) {
        return generateToken(user, jwtTokenProperties.getJwtRefreshExpiration());
    }

    public JwtTokenPair generateTokenPair(User user) {
        String accessToken = generateAccessToken(user);
        String refreshToken = generateRefreshToken(user);
        return new JwtTokenPair(accessToken, refreshToken);
    }

    public TokenDetails generateTokenDetails(User user) {
        String accessToken = generateAccessToken(user);
        String refreshToken = generateRefreshToken(user);
        return tokenTransformer.tokenStoreBuilder(accessToken, refreshToken, user);
    }


    public String generateToken(User user, long expirationTime) {
        Date currentDate = new Date();
        Date expiryDate = new Date(currentDate.getTime() + convertToMs(expirationTime));
        return Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(currentDate)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtTokenProperties.getJwtSecret())
                .compact();
    }

    private long convertToMs(long expirationTime) {
        return expirationTime * 1000;
    }

}
