package com.example.innowise_internship.validator;

import com.example.innowise_internship.handler.InvalidTokenException;
import com.example.innowise_internship.properties.JwtTokenProperties;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JwtValidator {

    private final JwtTokenProperties jwtTokenProperties;

    public JwtValidator(JwtTokenProperties jwtTokenProperties) {
        this.jwtTokenProperties = jwtTokenProperties;
    }

    public boolean validateJwtToken(String jwt) {
        try {
            Jwts.parser()
                    .setSigningKey(jwtTokenProperties.getJwtSecret())
                    .parseClaimsJws(jwt);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            log.error("Authentication Error" + e.getMessage());
            throw new InvalidTokenException("Token is invalid", e);
        }
    }

    public String getUserNameFromJwtToken(String jwt) {
        return Jwts.parser()
                .setSigningKey(jwtTokenProperties.getJwtSecret())
                .parseClaimsJws(jwt)
                .getBody()
                .getSubject();
    }
}
