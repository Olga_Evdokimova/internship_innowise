package com.example.innowise_internship.handler;

public class MimeMessageTemplateException extends RuntimeException {
    public MimeMessageTemplateException() {
    }

    public MimeMessageTemplateException(String message) {
        super(message);
    }

    public MimeMessageTemplateException(String message, Throwable cause) {
        super(message, cause);
    }

    public MimeMessageTemplateException(Throwable cause) {
        super(cause);
    }

    public MimeMessageTemplateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
