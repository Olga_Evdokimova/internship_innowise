package com.example.innowise_internship.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {NotFoundException.class})
    public ResponseEntity<IncorrectData> notFoundException(
            Exception exception) {
        log.error("Message: " + exception.getMessage());
        IncorrectData data = new IncorrectData();
        data.setMessage(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {ForbiddenException.class})
    public ResponseEntity<IncorrectData> forbiddenException(
            Exception exception) {
        log.error("Message: " + exception.getMessage());
        IncorrectData data = new IncorrectData();
        data.setMessage(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {MimeMessageTemplateException.class})
    public ResponseEntity<IncorrectData> emailException(
            Exception exception) {
        log.error("Message: " + exception.getMessage());
        IncorrectData data = new IncorrectData();
        data.setMessage(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<IncorrectData> handlerException(
            Exception exception) {
        log.error("Message: " + exception.getMessage());
        IncorrectData data = new IncorrectData();
        data.setMessage(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.BAD_REQUEST);
    }
}
