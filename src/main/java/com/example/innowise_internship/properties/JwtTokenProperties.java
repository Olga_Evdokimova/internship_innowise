package com.example.innowise_internship.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtTokenProperties {
    private String jwtSecret;
    private long jwtAccessExpiration;
    private long jwtRefreshExpiration;
}
