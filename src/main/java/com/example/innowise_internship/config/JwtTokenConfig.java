package com.example.innowise_internship.config;

import com.example.innowise_internship.properties.JwtTokenProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JwtTokenConfig {

    @Bean
    @ConfigurationProperties(prefix = "jwt.token")
    public JwtTokenProperties getJwtTokenProperties() {
        return new JwtTokenProperties();
    }
}
