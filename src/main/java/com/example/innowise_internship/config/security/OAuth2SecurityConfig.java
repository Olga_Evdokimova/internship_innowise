package com.example.innowise_internship.config.security;

import com.example.innowise_internship.config.security.oauth2.OAuth2SuccessHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;


@Profile("oauth2")
@Configuration
public class OAuth2SecurityConfig extends WebSecurityConfig{

    private final OAuth2SuccessHandler oAuth2SuccessHandler;

    public OAuth2SecurityConfig(OAuth2SuccessHandler oAuth2SuccessHandler) {
        this.oAuth2SuccessHandler = oAuth2SuccessHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http
                .oauth2Login()
                .loginPage("/login")
                .authorizationEndpoint()
                .baseUri("/api/oauth2/authorization")
                .and()
                .loginProcessingUrl("/api/login/oauth2/code/*")
                .successHandler(oAuth2SuccessHandler)
                .and()
                .authorizeRequests()
                .anyRequest()
                .authenticated();

    }


}
