package com.example.innowise_internship.config.security.oauth2;


import com.example.innowise_internship.properties.JwtTokenProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import java.util.Arrays;


@Profile("oauth2")
@Configuration
public class OAuth2TokenConfig {

    private final JwtTokenProperties jwtTokenProperties;
    private final JwtTokenEnhancer jwtTokenEnhancer;

    @Value("${keyPair.privateKey}")
    private String privateKey;

    public OAuth2TokenConfig(JwtTokenProperties jwtTokenProperties, JwtTokenEnhancer jwtTokenEnhancer) {
        this.jwtTokenProperties = jwtTokenProperties;
        this.jwtTokenEnhancer = jwtTokenEnhancer;
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(privateKey);
        return converter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore());
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setTokenEnhancer(jwtAccessTokenConverter());
        tokenServices.setAccessTokenValiditySeconds((int) jwtTokenProperties.getJwtAccessExpiration());
        tokenServices.setRefreshTokenValiditySeconds((int) jwtTokenProperties.getJwtRefreshExpiration());
        return tokenServices;
    }

    @Bean
    @Primary
    public TokenEnhancer tokenEnhancer(){
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(jwtAccessTokenConverter(), jwtTokenEnhancer));
        return tokenEnhancerChain;
    }
}