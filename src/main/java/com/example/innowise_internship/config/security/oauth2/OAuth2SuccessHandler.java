package com.example.innowise_internship.config.security.oauth2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@Profile("oauth2")
@Slf4j
@Component
public class OAuth2SuccessHandler implements AuthenticationSuccessHandler {
    public static final String SUCCESS_LOGIN_WITH_OAUTH_2_BY_TEMPLATE = "Success login with oauth2 by %s ";
    private final UserDetailsService userDetailsService;

    public OAuth2SuccessHandler(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                        Authentication authentication) throws IOException, ServletException {
        DefaultOidcUser oidcUser = (DefaultOidcUser) authentication.getPrincipal();
        User user = (User) userDetailsService.loadUserByUsername(oidcUser.getEmail());
        log.info(String.format(SUCCESS_LOGIN_WITH_OAUTH_2_BY_TEMPLATE, user.getUsername()));
        httpServletResponse.setStatus(200);
    }
}
