package com.example.innowise_internship.config.security.oauth2;


import com.example.innowise_internship.properties.JwtTokenProperties;
import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.builders.ClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;


@Profile("oauth2")
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final TokenEnhancer tokenEnhancer;
    private final OAuth2ClientProperties oAuth2ClientProperties;
    private final JwtTokenProperties jwtTokenProperties;
    private final UserDetailsService userDetails;
    private final DefaultTokenServices tokenServices;

    public AuthorizationServerConfig(AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder,
                                     TokenEnhancer tokenEnhancer, OAuth2ClientProperties oAuth2ClientProperties,
                                     JwtTokenProperties jwtTokenProperties, UserDetailsService userDetails,
                                     DefaultTokenServices tokenServices) {
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenEnhancer = tokenEnhancer;
        this.oAuth2ClientProperties = oAuth2ClientProperties;
        this.jwtTokenProperties = jwtTokenProperties;
        this.userDetails = userDetails;
        this.tokenServices = tokenServices;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        InMemoryClientDetailsServiceBuilder builder = clients.inMemory();
        this.addClients(builder);
    }

    private ClientDetailsServiceBuilder<?> addClients(ClientDetailsServiceBuilder<?> builder) {
        oAuth2ClientProperties.getRegistration().forEach((key, value) ->
                builder.withClient(value.getClientId())
                        .secret(value.getClientSecret() != null ? passwordEncoder.encode(value.getClientSecret()) : null)
                        .scopes("any")
                        .authorizedGrantTypes("password", "refresh_token")
                        .accessTokenValiditySeconds((int)jwtTokenProperties.getJwtAccessExpiration())
                        .refreshTokenValiditySeconds((int)jwtTokenProperties.getJwtRefreshExpiration()));
        return builder;
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetails)
                .tokenServices(tokenServices)
                .tokenEnhancer(tokenEnhancer);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security
                .tokenKeyAccess("permitAll()")
                .checkTokenAccess("isAuthenticated()");
    }

}