package com.example.innowise_internship.transformer;

import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import org.springframework.stereotype.Component;

@Component
public class TokenTransformer {

    public TokenDetails tokenStoreBuilder(String accessToken, String refreshToken, User user) {
        return TokenDetails.builder()
                .withRefreshToken(refreshToken)
                .withAccessToken(accessToken)
                .withUser(user)
                .build();
    }
}
