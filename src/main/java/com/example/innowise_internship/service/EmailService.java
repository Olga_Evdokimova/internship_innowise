package com.example.innowise_internship.service;

import com.example.innowise_internship.email.EmailMessageTemplate;
import com.example.innowise_internship.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class EmailService {

    private final JavaMailSender mailSender;
    private final EmailMessageTemplate emailMessageTemplate;

    public EmailService(JavaMailSender mailSender, EmailMessageTemplate emailMessageTemplate) {
        this.mailSender = mailSender;
        this.emailMessageTemplate = emailMessageTemplate;
    }

    public void sendEmailToAdmins(List<User> admins, Map<String, String> templateInfo) {
        admins.stream()
                .map(User::getEmail)
                .map(email -> emailMessageTemplate.prepareMessage(templateInfo, email))
                .forEach(this::sendEmail);
    }

    private void sendEmail(MimeMessage message) {
        mailSender.send(message);
    }

}
