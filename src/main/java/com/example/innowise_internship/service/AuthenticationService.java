package com.example.innowise_internship.service;

import com.example.innowise_internship.dto.JwtResponse;
import com.example.innowise_internship.dto.LoginRequest;
import com.example.innowise_internship.dto.TokenRefreshRequest;
import com.example.innowise_internship.dto.user.UserDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
public class AuthenticationService {

    private final UserService userService;
    private final TokenService tokenService;
    private final NotificationService notificationService;
    private final AuthenticationManager authenticationManager;


    public AuthenticationService(UserService userService, TokenService tokenService,
                                 NotificationService notificationService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.notificationService = notificationService;
        this.authenticationManager = authenticationManager;
    }

    public JwtResponse login(LoginRequest loginRequest) {
        authenticate(loginRequest);
        notificationService.sendNotificationToAdminsIfUserIsLate(loginRequest);
        User user = userService.getUserByEmail(loginRequest.getLogin());
        TokenDetails tokenDetails = tokenService.generateTokenDetails(user);
        tokenService.save(tokenDetails);
        return new JwtResponse(tokenDetails.getAccessToken(), tokenDetails.getRefreshToken(), user.getId());
    }

    private void authenticate(LoginRequest loginRequest){
        authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(
                        loginRequest.getLogin(),
                        loginRequest.getPassword()));
    }

    public UserDto register(UserRegisterDto userRegisterDTO) {
        UserDto userDto = userService.saveUser(userRegisterDTO);
        log.info("IN register - user: {} successfully registered", userDto);
        return userDto;
    }

    @Transactional
    public JwtResponse refresh(TokenRefreshRequest requestRefreshToken) {
        TokenDetails tokens = tokenService.refresh(requestRefreshToken);
        return new JwtResponse(tokens.getAccessToken(), tokens.getRefreshToken(), tokens.getUser().getId());
    }

    @Transactional
    public void logout(String refreshToken) {
        TokenDetails tokenDetails = tokenService.getTokenDetailsByRefresh(refreshToken);
        tokenService.deleteRefreshToken(tokenDetails);
    }

}
