package com.example.innowise_internship.service;

import com.example.innowise_internship.dto.role.RoleDto;
import com.example.innowise_internship.handler.RoleNotFoundException;
import com.example.innowise_internship.mapper.RoleMapper;
import com.example.innowise_internship.repository.RoleRepository;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    public static final String ROLE_NOT_FOUND_TEMPLATE = "Role with id %s not found";
    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    public RoleService(RoleRepository roleRepository, RoleMapper roleMapper) {
        this.roleRepository = roleRepository;
        this.roleMapper = roleMapper;
    }

    public RoleDto getRoleById(Long roleId) {
        return roleRepository.findById(roleId)
                .map(roleMapper::toRoleDto)
                .orElseThrow(() -> new RoleNotFoundException(String.format(ROLE_NOT_FOUND_TEMPLATE, roleId)));
    }

}
