package com.example.innowise_internship.service;

import com.example.innowise_internship.config.security.reader.SecurityContextReader;
import com.example.innowise_internship.dto.user.UserDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.handler.UserNotFoundException;
import com.example.innowise_internship.mapper.UserMapper;
import com.example.innowise_internship.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class UserService {

    public static final String USER_BY_ID_NOT_FOUND_MESSAGE_TEMPLATE = "There is no such user with id: %s";
    public static final String USER_BY_EMAIL_NOT_FOUND_MESSAGE_TEMPLATE = "There is no such user with email: %s";
    public static final String ADMINS_FOR_USER_NOT_FOUND_MESSAGE_TEMPLATE = "There is no such admins for user with id: %s";
    public static final String ROLE_USER = "ROLE_USER";

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final SecurityContextReader securityContextReader;

    public UserService(UserRepository userRepository, UserMapper userMapper, SecurityContextReader securityContextReader) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.securityContextReader = securityContextReader;
    }

    public List<User> findAdminsForUser(User user) {
        return userRepository.findAllAById(user.getId())
                .orElseThrow(() -> new UserNotFoundException(String.format(ADMINS_FOR_USER_NOT_FOUND_MESSAGE_TEMPLATE, user.getId())));
    }

    public boolean isLateUser(User user) {
        return LocalDateTime.now().isAfter(user.getStartWorkHour());
    }

    @Transactional
    public void assignAdmin(Long userId) {
        User user = getUserById(userId);
        User admin = userFromContextById();
        user.getAdmins().add(admin);
        userRepository.save(user);
    }


    public UserDto saveUser(UserRegisterDto userRegisterDto) {
        User user = userMapper.toUserWithEncodePassword(userRegisterDto);
        userRepository.save(user);
        return userMapper.toUserDto(user);
    }

    @Transactional
    public void rollback(LocalDateTime date) {
        userRepository.rollback(date);
    }

    public User userFromContextById() {
        User user = (User) securityContextReader.getAuthenticationFromSecurityContext().getPrincipal();
        Long id = user.getId();
        return getUserById(id);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(String.format(USER_BY_EMAIL_NOT_FOUND_MESSAGE_TEMPLATE, email)));
    }

    public User getUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(String.format(USER_BY_ID_NOT_FOUND_MESSAGE_TEMPLATE, userId)));
    }

}
