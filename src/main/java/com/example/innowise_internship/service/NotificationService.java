package com.example.innowise_internship.service;

import com.example.innowise_internship.dto.LoginRequest;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.utils.TemplateUtil;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.innowise_internship.service.UserService.ROLE_USER;

@Service
public class NotificationService {
    private final UserService userService;
    private final EmailService emailService;

    public NotificationService(UserService userService, EmailService emailService) {
        this.userService = userService;
        this.emailService = emailService;
    }

    public void sendNotificationToAdminsIfUserIsLate(LoginRequest loginRequest) {
        User user = userService.getUserByEmail(loginRequest.getLogin());
        if (user.getUserRole().getName().equals(ROLE_USER) && userService.isLateUser(user)) {
            List<User> admins = userService.findAdminsForUser(user);
            emailService.sendEmailToAdmins(admins, TemplateUtil.prepareTemplateInfo(user));
        }
    }
}
