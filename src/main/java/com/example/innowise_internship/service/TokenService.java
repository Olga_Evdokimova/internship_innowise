package com.example.innowise_internship.service;

import com.example.innowise_internship.config.security.reader.SecurityContextReader;
import com.example.innowise_internship.dto.TokenRefreshRequest;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.JwtTokenPair;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.handler.TokenRefreshNotFoundException;
import com.example.innowise_internship.provider.TokenHandler;
import com.example.innowise_internship.repository.TokenRepository;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class TokenService {

    public static final String REFRESH_TOKEN_NOT_FOUND_TEMPLATE = "Refresh token %s not found";

    private final TokenRepository tokenRepository;
    private final TokenHandler tokenHandler;
    private final SecurityContextReader securityContextReader;

    public TokenService(TokenRepository tokenRepository, TokenHandler tokenHandler, SecurityContextReader securityContextReader) {
        this.tokenRepository = tokenRepository;
        this.tokenHandler = tokenHandler;
        this.securityContextReader = securityContextReader;
    }

    public TokenDetails refresh(TokenRefreshRequest requestRefreshToken) {
        TokenDetails tokenDetails = getTokenDetailsByRefresh(requestRefreshToken.getRefreshToken());
        isConfirmedUser(tokenDetails);
        return update(tokenDetails);
    }

    public TokenDetails update(TokenDetails tokenDetails) {
        JwtTokenPair tokenPair = tokenHandler.generateTokenPair(tokenDetails.getUser());
        tokenDetails.setAccessToken(tokenPair.getAccessToken());
        tokenDetails.setRefreshToken(tokenPair.getRefreshToken());
        return save(tokenDetails);
    }

    public TokenDetails save(TokenDetails tokenDetails) {
        return tokenRepository.save(tokenDetails);
    }

    public void deleteRefreshToken(TokenDetails tokenDetails) {
        isConfirmedUser(tokenDetails);
        tokenRepository.delete(tokenDetails);
    }

    private void isConfirmedUser(@NotNull TokenDetails tokenDetails) {
        Authentication authentication = securityContextReader.getAuthenticationFromSecurityContext();
        if (!Objects.equals(tokenDetails.getUser().getEmail(), authentication.getName())) {
            throw new RuntimeException("Users in system are not matched with refresh token!");
        }
    }

    public TokenDetails generateTokenDetails(User user) {
        return tokenHandler.generateTokenDetails(user);
    }


    public TokenDetails getTokenDetailsByRefresh(String refreshToken) {
        return tokenRepository.findByRefreshToken(refreshToken)
                .orElseThrow(() -> new TokenRefreshNotFoundException(String.format(REFRESH_TOKEN_NOT_FOUND_TEMPLATE, refreshToken)));
    }

}
