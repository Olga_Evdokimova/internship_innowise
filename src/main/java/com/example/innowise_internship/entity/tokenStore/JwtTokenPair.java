package com.example.innowise_internship.entity.tokenStore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder(setterPrefix = "with", toBuilder = true)
public class JwtTokenPair {

    private String accessToken;
    private String refreshToken;
}
