package com.example.innowise_internship.mapper;

import com.example.innowise_internship.dto.user.UserDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.entity.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final RoleMapper roleMapper;

    private final PasswordEncoder passwordEncoder;

    public UserMapper(RoleMapper roleMapper, PasswordEncoder passwordEncoder) {
        this.roleMapper = roleMapper;
        this.passwordEncoder = passwordEncoder;
    }

    public User toUserWithEncodePassword(UserRegisterDto userRegisterDto) {
        return getUserBuilder(userRegisterDto)
                .withPassword(passwordEncoder.encode(userRegisterDto.getPassword()))
                .build();
    }

    public User toUser(UserRegisterDto userRegisterDto) {
        return getUserBuilder(userRegisterDto)
                .withPassword(userRegisterDto.getPassword())
                .build();
    }

    private User.UserBuilder getUserBuilder(UserRegisterDto userRegisterDto) {
        return User.builder()
                .withFirstName(userRegisterDto.getFirstName())
                .withLastName(userRegisterDto.getLastName())
                .withUserRole(roleMapper.toRole(userRegisterDto.getRoleDto()))
                .withLogin(userRegisterDto.getLogin())
                .withEmail(userRegisterDto.getEmail())
                .withStartWorkHour(userRegisterDto.getStartWorkHour())
                .withEndWorkHour(userRegisterDto.getEndWorkHour());
    }


    public UserDto toUserDto(User user) {
        return UserDto.builder().withId(user.getId())
                .withFirstName(user.getFirstName())
                .withLastName(user.getLastName())
                .withRoleDto(roleMapper.toRoleDto(user.getUserRole()))
                .build();
    }
}
