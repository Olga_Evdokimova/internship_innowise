package com.example.innowise_internship.mapper;

import com.example.innowise_internship.dto.role.RoleDto;
import com.example.innowise_internship.entity.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoleMapper {

    public RoleDto toRoleDto(Role role) {
        return RoleDto.builder()
                .withId(role.getId())
                .withName(role.getName())
                .build();
    }

    Role toRole(RoleDto roleDto) {

        return Role.builder()
                .withId(roleDto.getId())
                .withName(roleDto.getName())
                .build();
    }
}
