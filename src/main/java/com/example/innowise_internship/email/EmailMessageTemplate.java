package com.example.innowise_internship.email;

import com.example.innowise_internship.handler.MimeMessageTemplateException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Slf4j
@Component
public class EmailMessageTemplate {
    private static final String SUBJECT = "Late entry notification";
    private static final String EMAIL_TEMPLATE = "email.ftl";

    private final JavaMailSender mailSender;
    private final Configuration templateConfiguration;
    private final String senderEmail;

    public EmailMessageTemplate(JavaMailSender mailSender, Configuration templateConfiguration, @Value("spring.mail.username") String senderEmail) {
        this.mailSender = mailSender;
        this.templateConfiguration = templateConfiguration;
        this.senderEmail = senderEmail;
    }

    public MimeMessage prepareMessage(Map<String, String> templateInfo, String email) {
        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());
            Template template = templateConfiguration.getTemplate(EMAIL_TEMPLATE);
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, templateInfo);
            helper.setFrom(senderEmail);
            helper.setSubject(SUBJECT);
            helper.setText(html, true);
            helper.setTo(email);
            return message;
        } catch (IOException | TemplateException | MessagingException e) {
            log.error(e.getMessage());
            throw new MimeMessageTemplateException("Something went wrong with your message in template" + templateInfo);

        }
    }
}
