package com.example.innowise_internship.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/oauth2")
public class OAuthController {

    @GetMapping
    public String get(){
        return "Hello, oauth2!";
    }

}
