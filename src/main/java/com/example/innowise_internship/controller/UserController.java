package com.example.innowise_internship.controller;

import com.example.innowise_internship.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping("admin/assign/{userId}")
    @ApiOperation("Assign admin")
    public void assignAdmin(@ApiParam(
            name = "UserId",
            type = "Long",
            value = "Id of the user",
            example = "1",
            required = true) @PathVariable Long userId) {
        userService.assignAdmin(userId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("rollback")
    @ApiOperation("Rollback data")
    public void rollbackByDate(@ApiParam(
            name = "Date",
            type = "LocalDateTime",
            value = "Date for rollback",
            required = true) @RequestBody LocalDateTime date) {
        userService.rollback(date);
    }


}
