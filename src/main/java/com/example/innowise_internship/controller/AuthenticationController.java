package com.example.innowise_internship.controller;

import com.example.innowise_internship.dto.JwtResponse;
import com.example.innowise_internship.dto.LoginRequest;
import com.example.innowise_internship.dto.TokenRefreshRequest;
import com.example.innowise_internship.dto.user.UserDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.service.AuthenticationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    private final AuthenticationService authenticationService;


    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    @ApiOperation("Authorize method")
    public JwtResponse login(@RequestBody LoginRequest loginRequest) {
        return authenticationService.login(loginRequest);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("user/signUp")
    @ApiOperation("Sign up users")
    public UserDto register(@RequestBody UserRegisterDto userRegisterDto) {
        return authenticationService.register(userRegisterDto);
    }


    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PutMapping("/refreshtoken")
    @ApiOperation("Refresh token")
    public JwtResponse refreshToken(@RequestBody TokenRefreshRequest refreshRequest) {
        return authenticationService.refresh(refreshRequest);
    }


    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @DeleteMapping("/logout")
    @ApiOperation("Logout")
    public void logout(@ApiParam(
            name = "RefreshToken",
            type = "String",
            value = "Put refresh token",
            required = true)
                       @RequestParam String refreshToken) {
        authenticationService.logout(refreshToken);
    }

}
