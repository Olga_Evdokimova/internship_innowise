package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.email.EmailMessageTemplate;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.service.EmailService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

@ExtendWith(MockitoExtension.class)
public class EmailServiceTest {
    public static final String EMAIL_TEMPLATE = "email.ftl";
    @Mock
    private JavaMailSender mailSender;
    @Mock
    private EmailMessageTemplate emailMessageTemplate;
    @InjectMocks
    private EmailService emailService;

    @Test
    void shouldSendEmail() {
        Map<String, String> templateInfo = new HashMap<>();
        List<User> admins = List.of(User.builder().withEmail("1").build());
        MimeMessage mimeMessageMock = Mockito.mock(MimeMessage.class);
        given(emailMessageTemplate.prepareMessage(templateInfo, "1")).willReturn(mimeMessageMock);

        emailService.sendEmailToAdmins(admins, templateInfo);

        then(emailMessageTemplate).should(only()).prepareMessage(templateInfo, "1");
        then(mailSender).should(only()).send(mimeMessageMock);
    }
}
