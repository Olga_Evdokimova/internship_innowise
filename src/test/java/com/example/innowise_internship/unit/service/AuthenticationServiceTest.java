package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.dto.JwtResponse;
import com.example.innowise_internship.dto.LoginRequest;
import com.example.innowise_internship.dto.TokenRefreshRequest;
import com.example.innowise_internship.dto.user.UserDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.service.AuthenticationService;
import com.example.innowise_internship.service.NotificationService;
import com.example.innowise_internship.service.TokenService;
import com.example.innowise_internship.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTest {
    public static final String DEFAULT_EMAIL = "email";
    public static final Long DEFAULT_ID = 0L;
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String REFRESH_TOKEN = "refreshToken";
    public static final JwtResponse JWT_RESPONSE = JwtResponse.builder()
            .withAccessToken(ACCESS_TOKEN)
            .withRefreshToken(REFRESH_TOKEN)
            .withUserId(DEFAULT_ID)
            .build();
    public static final User DEFAULT_USER = User.builder()
            .withId(DEFAULT_ID)
            .build();
    public static final TokenRefreshRequest TOKEN_REFRESH_REQUEST = TokenRefreshRequest.builder()
            .withRefreshToken(REFRESH_TOKEN)
            .build();


    public static final TokenDetails TOKEN_DETAILS = TokenDetails.builder()
            .withUser(DEFAULT_USER)
            .withAccessToken(ACCESS_TOKEN)
            .withRefreshToken(REFRESH_TOKEN)
            .build();
    @Mock
    private UserService userService;

    @Mock
    private TokenService tokenService;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private AuthenticationService authenticationService;


    @Test
    void shouldReturnAppropriateJwtResponseOnLogin() {
        final LoginRequest loginRequest = LoginRequest.builder()
                .withLogin(DEFAULT_EMAIL)
                .build();

        final TokenDetails tokenDetails = TokenDetails.builder()
                .withAccessToken(ACCESS_TOKEN)
                .withRefreshToken(REFRESH_TOKEN)
                .build();

        given(userService.getUserByEmail(DEFAULT_EMAIL)).willReturn(DEFAULT_USER);
        given(tokenService.generateTokenDetails(DEFAULT_USER)).willReturn(tokenDetails);

        assertEquals(JWT_RESPONSE, authenticationService.login(loginRequest));

        then(notificationService).should(only()).sendNotificationToAdminsIfUserIsLate(loginRequest);
        then(userService).should(times(1)).getUserByEmail(DEFAULT_EMAIL);
        then(userService).shouldHaveNoMoreInteractions();
        then(tokenService).should(times(1)).generateTokenDetails(DEFAULT_USER);
        then(tokenService).should(times(1)).save(tokenDetails);
        then(tokenService).shouldHaveNoMoreInteractions();
    }

    @Test
    void shouldReturnUserDto_WhenRegisterUserRegisterDto() {
        UserRegisterDto userRegisterDto = UserRegisterDto.builder()
                .withLogin(DEFAULT_EMAIL)
                .build();
        UserDto userDto = UserDto.builder()
                .withId(DEFAULT_ID)
                .build();

        given(userService.saveUser(userRegisterDto)).willReturn(userDto);

        assertEquals(userDto, authenticationService.register(userRegisterDto));

        then(userService).should(only()).saveUser(userRegisterDto);
        then(tokenService).shouldHaveNoInteractions();

    }

    @Test
    void shouldReturnAppropriateJwtResponseOnRefresh() {

        given(tokenService.refresh(TOKEN_REFRESH_REQUEST)).willReturn(TOKEN_DETAILS);
        assertEquals(JWT_RESPONSE, authenticationService.refresh(TOKEN_REFRESH_REQUEST));

        then(tokenService).should(only()).refresh(TOKEN_REFRESH_REQUEST);
        then(userService).shouldHaveNoInteractions();
    }

    @Test
    void shouldDeleteRefreshTokenOnLogout() {

        given(tokenService.getTokenDetailsByRefresh(REFRESH_TOKEN)).willReturn(TOKEN_DETAILS);

        authenticationService.logout(REFRESH_TOKEN);

        then(tokenService).should(times(1)).getTokenDetailsByRefresh(REFRESH_TOKEN);
        then(tokenService).should(times(1)).deleteRefreshToken(TOKEN_DETAILS);
        then(tokenService).shouldHaveNoMoreInteractions();
        then(userService).shouldHaveNoInteractions();

    }
}
