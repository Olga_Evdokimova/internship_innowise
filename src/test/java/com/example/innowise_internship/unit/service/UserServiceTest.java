package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.dto.user.UserDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.handler.UserNotFoundException;
import com.example.innowise_internship.mapper.UserMapper;
import com.example.innowise_internship.repository.UserRepository;
import com.example.innowise_internship.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.innowise_internship.service.UserService.USER_BY_EMAIL_NOT_FOUND_MESSAGE_TEMPLATE;
import static com.example.innowise_internship.service.UserService.USER_BY_ID_NOT_FOUND_MESSAGE_TEMPLATE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    public static final Long DEFAULT_ID = 0L;
    public static final String DEFAULT_EMAIL = "email";
    public static final String DEFAULT_ROLE_DTO = "DEFAULT_ROLE_DTO";
    public static final String DEFAULT_ROLE = "DEFAULT_ROLE";
    public static final String DEFAULT_PASSWORD = "password";

    public static final User DEFAULT_USER = User.builder()
            .withId(DEFAULT_ID)
            .withEmail(DEFAULT_EMAIL)
            .build();
    public static final User DEFAULT_ADMIN = User.builder()
            .withId(DEFAULT_ID)
            .withLogin(DEFAULT_EMAIL)
            .build();

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserMapper userMapper;
    @InjectMocks
    private UserService userService;


    @Test
    void shouldReturnUserById() {
        final Long id = 0L;
        given(userRepository.findById(id)).willReturn(Optional.ofNullable(DEFAULT_USER));

        User user = userService.getUserById(DEFAULT_ID);
        assertEquals(user, DEFAULT_USER);

        then(userRepository).should(only()).findById(DEFAULT_ID);

        then(userMapper).shouldHaveNoInteractions();
    }

    @Test
    void shouldReturnUserNotFoundException_WhenNotFound_ById() {
        final Long id = 0L;
        given(userRepository.findById(id)).willReturn(Optional.empty());

        final UserNotFoundException exception = assertThrows(UserNotFoundException.class,
                () -> userService.getUserById(id));

        assertEquals(String.format(USER_BY_ID_NOT_FOUND_MESSAGE_TEMPLATE, id), exception.getMessage());

        then(userRepository).should(only()).findById(id);

        then(userMapper).shouldHaveNoInteractions();
    }

    @Test
    void shouldReturnUserNotFoundException_WhenNotFound_ByLogin() {
        given(userRepository.findByEmail(DEFAULT_EMAIL)).willReturn(Optional.empty());

        final UserNotFoundException exception = assertThrows(UserNotFoundException.class,
                () -> userService.getUserByEmail(DEFAULT_EMAIL));

        assertEquals(String.format(USER_BY_EMAIL_NOT_FOUND_MESSAGE_TEMPLATE, DEFAULT_EMAIL), exception.getMessage());

        then(userRepository).should(only()).findByEmail(DEFAULT_EMAIL);

        then(userMapper).shouldHaveNoInteractions();
    }

    @Test
    void shouldReturnUserByLogin() {
        given(userRepository.findByEmail(DEFAULT_EMAIL)).willReturn(Optional.ofNullable(DEFAULT_USER));

        User user = userService.getUserByEmail(DEFAULT_EMAIL);
        assertEquals(user, DEFAULT_USER);

        then(userRepository).should(only()).findByEmail(DEFAULT_EMAIL);
    }

    @Test
    void shouldSaveUser() {
        UserRegisterDto userRegisterDto = UserRegisterDto
                .builder()
                .withPassword(DEFAULT_PASSWORD)
                .build();
        UserDto userDto = new UserDto();
        User user = new User();

        given(userMapper.toUserWithEncodePassword(userRegisterDto)).willReturn(user);
        given(userMapper.toUserDto(user)).willReturn(userDto);

        assertEquals(userDto, userService.saveUser(userRegisterDto));

        then(userRepository).should(only()).save(user);
        then(userMapper).should(times(1)).toUserWithEncodePassword(userRegisterDto);
        then(userMapper).should(times(1)).toUserDto(user);
        then(userMapper).shouldHaveNoMoreInteractions();
    }

}
