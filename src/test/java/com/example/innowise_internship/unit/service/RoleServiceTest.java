package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.dto.role.RoleDto;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.handler.RoleNotFoundException;
import com.example.innowise_internship.mapper.RoleMapper;
import com.example.innowise_internship.repository.RoleRepository;
import com.example.innowise_internship.service.RoleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.innowise_internship.service.RoleService.ROLE_NOT_FOUND_TEMPLATE;
import static com.example.innowise_internship.unit.service.UserServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTest {

    public static final RoleDto ROLE_DTO = RoleDto.builder().withName(DEFAULT_ROLE_DTO).build();
    public static final Role ROLE = Role.builder().withName(DEFAULT_ROLE).build();

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private RoleMapper roleMapper;

    @InjectMocks
    private RoleService roleService;


    @Test
    void shouldReturnRoleById() {
        given(roleRepository.findById(DEFAULT_ID)).willReturn(Optional.ofNullable(ROLE));
        given(roleMapper.toRoleDto(ROLE)).willReturn(ROLE_DTO);
        assertEquals(ROLE_DTO, roleService.getRoleById(DEFAULT_ID));

        then(roleRepository).should(only()).findById(DEFAULT_ID);
        then(roleMapper).should(only()).toRoleDto(ROLE);
    }

    @Test
    void shouldReturnRoleNotFoundException_WhenNotFound_ById() {
        given(roleRepository.findById(DEFAULT_ID)).willReturn(Optional.empty());

        final RoleNotFoundException exception = assertThrows(RoleNotFoundException.class,
                () -> roleService.getRoleById(DEFAULT_ID));

        assertEquals(String.format(ROLE_NOT_FOUND_TEMPLATE, DEFAULT_ID), exception.getMessage());

        then(roleRepository).should(only()).findById(DEFAULT_ID);
        then(roleMapper).shouldHaveNoInteractions();
    }

}
