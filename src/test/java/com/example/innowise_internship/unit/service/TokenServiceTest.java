package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.entity.tokenStore.JwtTokenPair;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.handler.TokenRefreshNotFoundException;
import com.example.innowise_internship.provider.TokenHandler;
import com.example.innowise_internship.repository.TokenRepository;
import com.example.innowise_internship.service.TokenService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.example.innowise_internship.service.TokenService.REFRESH_TOKEN_NOT_FOUND_TEMPLATE;
import static com.example.innowise_internship.unit.service.AuthenticationServiceTest.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

@ExtendWith(MockitoExtension.class)
public class TokenServiceTest {

    public static final String ACCESS_TOKEN_NEW = "accessTokenNew";
    public static final String REFRESH_TOKEN_NEW = "refreshTokenNew";

    public static final TokenDetails TOKEN_DETAILS_REFRESHED = TokenDetails.builder()
            .withUser(DEFAULT_USER)
            .withAccessToken(ACCESS_TOKEN_NEW)
            .withRefreshToken(REFRESH_TOKEN_NEW)
            .build();
    public static final JwtTokenPair JWT_TOKEN_PAIR = JwtTokenPair.builder()
            .withAccessToken(ACCESS_TOKEN_NEW)
            .withRefreshToken(REFRESH_TOKEN_NEW)
            .build();

    @Mock
    private TokenRepository tokenRepository;
    @Mock
    private TokenHandler tokenHandler;

    @InjectMocks
    private TokenService tokenService;


    @Test
    void shouldReturnTokenDetailsOnUpdate() {

        given(tokenHandler.generateTokenPair(TOKEN_DETAILS.getUser())).willReturn(JWT_TOKEN_PAIR);
        given(tokenRepository.save(TOKEN_DETAILS_REFRESHED)).willReturn(TOKEN_DETAILS_REFRESHED);

        assertEquals(TOKEN_DETAILS_REFRESHED, tokenService.update(TOKEN_DETAILS));

        then(tokenHandler).should(only()).generateTokenPair(TOKEN_DETAILS.getUser());
        then(tokenRepository).should(only()).save(TOKEN_DETAILS_REFRESHED);
    }

    @Test
    void shouldReturnTokenDetails_WhenFindByRefreshToken() {
        given(tokenRepository.findByRefreshToken(REFRESH_TOKEN)).willReturn(Optional.ofNullable(TOKEN_DETAILS));

        assertEquals(TOKEN_DETAILS, tokenService.getTokenDetailsByRefresh(REFRESH_TOKEN));

        then(tokenRepository).should(only()).findByRefreshToken(REFRESH_TOKEN);
        then(tokenHandler).shouldHaveNoInteractions();
    }

    @Test
    void shouldReturnTokenRefreshNotFoundException_WhenFoundByRefreshToken() {
        given(tokenRepository.findByRefreshToken(REFRESH_TOKEN)).willReturn(Optional.empty());

        final TokenRefreshNotFoundException exception = assertThrows(TokenRefreshNotFoundException.class,
                () -> tokenService.getTokenDetailsByRefresh(REFRESH_TOKEN));
        assertEquals(String.format(REFRESH_TOKEN_NOT_FOUND_TEMPLATE, REFRESH_TOKEN), exception.getMessage());

        then(tokenRepository).should(only()).findByRefreshToken(REFRESH_TOKEN);
        then(tokenHandler).shouldHaveNoInteractions();
    }

    @Test
    void shouldGenerateTokenDetailsByUser() {
        given(tokenHandler.generateTokenDetails(DEFAULT_USER)).willReturn(TOKEN_DETAILS);

        assertEquals(TOKEN_DETAILS, tokenService.generateTokenDetails(DEFAULT_USER));

        then(tokenHandler).should(only()).generateTokenDetails(DEFAULT_USER);
        then(tokenRepository).shouldHaveNoInteractions();
    }

}
