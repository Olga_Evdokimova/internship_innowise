package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.email.EmailMessageTemplate;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.util.ReflectionTestUtils;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

@ExtendWith(MockitoExtension.class)
public class EmailMessageTemplateTest {
    public static final String EMAIL_TEMPLATE = "email.ftl";
    private static final String SUBJECT = "Late entry notification";
    @Mock
    private JavaMailSender mailSender;
    @Mock
    private Configuration templateConfiguration;


    @InjectMocks
    private EmailMessageTemplate emailMessageTemplate;

    @Test
    void shouldPrepareMessage() throws IOException, MessagingException {
        Map<String, String> templateInfo = new HashMap<>();
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");
        Session session = Session.getDefaultInstance(properties, null);
        MimeMessage mimeMessage = new MimeMessage(session);
        Template template = Template.getPlainTextTemplate("a", "b", new Configuration());
        ReflectionTestUtils.setField(emailMessageTemplate, "senderEmail", "test");
        given(mailSender.createMimeMessage()).willReturn(mimeMessage);
        given(templateConfiguration.getTemplate(EMAIL_TEMPLATE)).willReturn(template);

        MimeMessage expected = emailMessageTemplate.prepareMessage(templateInfo, "email");

        assertEquals(expected.getSubject(), SUBJECT);

        then(mailSender).should(only()).createMimeMessage();
        then(templateConfiguration).should(only()).getTemplate(EMAIL_TEMPLATE);


    }
}
