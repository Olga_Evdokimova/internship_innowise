package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.dto.LoginRequest;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.service.EmailService;
import com.example.innowise_internship.service.NotificationService;
import com.example.innowise_internship.service.UserService;
import com.example.innowise_internship.utils.TemplateUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.innowise_internship.service.UserService.ROLE_USER;
import static com.example.innowise_internship.unit.service.UserServiceTest.DEFAULT_EMAIL;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceTest {

    public static final User DEFAULT_USER = User.builder()
            .withLogin(DEFAULT_EMAIL)
            .withFirstName("Name")
            .withUserRole(Role.builder().withName(ROLE_USER).build())
            .withStartWorkHour(LocalDateTime.now())
            .build();
    @Mock
    private UserService userService;
    @Mock
    private EmailService emailService;
    @InjectMocks
    private NotificationService notificationService;

    @Test
    void shouldSendNotificationToAdminsIfUserIsLate() {
        final LoginRequest loginRequest = LoginRequest.builder()
                .withLogin(DEFAULT_EMAIL)
                .build();
        List<User> admins = List.of(User.builder().withEmail("1").build());
        given(userService.getUserByEmail(DEFAULT_EMAIL)).willReturn(DEFAULT_USER);
        given(userService.isLateUser(DEFAULT_USER)).willReturn(true);
        given(userService.findAdminsForUser(DEFAULT_USER)).willReturn(admins);

        notificationService.sendNotificationToAdminsIfUserIsLate(loginRequest);

        then(userService).should(times(1)).getUserByEmail(DEFAULT_EMAIL);
        then(userService).should(times(1)).isLateUser(DEFAULT_USER);
        then(userService).should(times(1)).findAdminsForUser(DEFAULT_USER);
        then(userService).shouldHaveNoMoreInteractions();
        then(emailService).should(only()).sendEmailToAdmins(admins, TemplateUtil.prepareTemplateInfo(DEFAULT_USER));


    }
}
