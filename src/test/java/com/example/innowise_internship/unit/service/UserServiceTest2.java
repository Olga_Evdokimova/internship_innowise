package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.mapper.UserMapper;
import com.example.innowise_internship.repository.UserRepository;
import com.example.innowise_internship.service.EmailService;
import com.example.innowise_internship.service.RoleService;
import com.example.innowise_internship.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;


@ExtendWith(MockitoExtension.class)
public class UserServiceTest2 {

    public static final LocalDateTime START_WORK_HOUR = LocalDateTime.of(2021, 07, 12, 10, 00, 00);
    public static final User DEFAULT_USER = User.builder()
            .withFirstName("string")
            .withUserRole(Role.builder().withId(2L).withName("ROLE_USER").build())
            .withStartWorkHour(START_WORK_HOUR)
            .build();

    @Mock
    private UserRepository userRepository;
    @Mock
    private UserMapper userMapper;
    @Mock
    private RoleService roleService;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private EmailService emailService;

    @InjectMocks
    private UserService userService;

    @Test
    void shouldHaveRoleUser_AndIfLate_SendEmailToAdmins() {
        List<User> admins = List.of(User.builder().withEmail("string1@gmail.com").build(),
                User.builder().withEmail("string2@gmail.com").build());
        given(userRepository.findAllAById(DEFAULT_USER.getId())).willReturn(Optional.of(admins));

        userService.findAdminsForUser(DEFAULT_USER);

        then(userRepository).should(only()).findAllAById(DEFAULT_USER.getId());
        then(userRepository).shouldHaveNoMoreInteractions();

        then(userMapper).shouldHaveNoInteractions();
        then(roleService).shouldHaveNoInteractions();
        then(passwordEncoder).shouldHaveNoInteractions();
    }

    @Test
    void shouldRollbackUser() {
        userService.rollback(START_WORK_HOUR);
        then(userRepository).should(only()).rollback(START_WORK_HOUR);
        then(userRepository).shouldHaveNoMoreInteractions();
    }

}
