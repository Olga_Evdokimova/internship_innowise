package com.example.innowise_internship.unit.service;

import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.repository.UserRepository;
import com.example.innowise_internship.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

@ExtendWith(MockitoExtension.class)
class UserDetailsServiceImplTest {

    public static final String DEFAULT_EMAIL = "email";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final User DEFAULT_USER = User.builder()
            .withEmail(DEFAULT_EMAIL)
            .withUserRole(Role.builder()
                    .withName(ROLE_ADMIN).build())
            .build();
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @Test
    void shouldReturnValidUserDetailsImplWithValidLogin() {
        given(userRepository.findByEmail(DEFAULT_EMAIL)).willReturn(Optional.ofNullable(DEFAULT_USER));

        User userDetails = userDetailsService.loadUserByUsername(DEFAULT_EMAIL);
        assertEquals(DEFAULT_USER, userDetails);

        then(userRepository).should(only()).findByEmail(DEFAULT_EMAIL);
    }

}