package com.example.innowise_internship.unit.tokenHandler;

import com.example.innowise_internship.entity.tokenStore.JwtTokenPair;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.properties.JwtTokenProperties;
import com.example.innowise_internship.provider.TokenHandler;
import com.example.innowise_internship.transformer.TokenTransformer;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static com.example.innowise_internship.unit.service.UserServiceTest.DEFAULT_USER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class TokenHandlerTest {

    JwtTokenProperties jwtTokenProperties = new JwtTokenProperties("SecretKey", 10000, 20000);

    @Mock
    private TokenTransformer tokenTransformer;

    @InjectMocks
    private TokenHandler tokenHandler;

    @Test
    void shouldGenerateTokenPairByUser() {
        TokenHandler tokenHandler = new TokenHandler(jwtTokenProperties, tokenTransformer);
        String accessToken = tokenHandler.generateAccessToken(DEFAULT_USER);
        String refreshToken = tokenHandler.generateRefreshToken(DEFAULT_USER);

        JwtTokenPair JWT_TOKEN_PAIR = JwtTokenPair.builder()
                .withAccessToken(accessToken)
                .withRefreshToken(refreshToken)
                .build();

        JwtTokenPair jwtTokenPair = tokenHandler.generateTokenPair(DEFAULT_USER);

        assertEquals(JWT_TOKEN_PAIR, jwtTokenPair);
    }

    @Test
    void shouldGenerateTokenDetailsByUser() {
        TokenHandler tokenHandler = new TokenHandler(jwtTokenProperties, tokenTransformer);
        String accessToken = tokenHandler.generateAccessToken(DEFAULT_USER);
        String refreshToken = tokenHandler.generateRefreshToken(DEFAULT_USER);
        TokenDetails tokenDetails = TokenDetails.builder().withAccessToken(accessToken)
                .withRefreshToken(refreshToken)
                .withUser(DEFAULT_USER)
                .build();
        given(tokenTransformer.tokenStoreBuilder(accessToken, refreshToken, DEFAULT_USER)).willReturn(tokenDetails);
        assertEquals(tokenDetails, tokenHandler.generateTokenDetails(DEFAULT_USER));
    }

    @Test
    void shouldGenerateTokenByUserAndExpirationTime() {
        TokenHandler tokenHandler = new TokenHandler(jwtTokenProperties, tokenTransformer);

        long expirationTime = jwtTokenProperties.getJwtAccessExpiration();
        Date currentDate = new Date();
        long expTime = expirationTime * 1000;
        Date expiryDate = new Date(currentDate.getTime() + expTime);
        String token = Jwts.builder()
                .setSubject(DEFAULT_USER.getUsername())
                .setIssuedAt(currentDate)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtTokenProperties.getJwtSecret())
                .compact();

        String actualToken = tokenHandler.generateToken(DEFAULT_USER, expirationTime);

        assertEquals(jwtParser(token), jwtParser(actualToken));
//        assertThat(jwtParser(actualToken)).isEqualToIgnoringGivenFields(jwtParser(token),"iat", "exp");

    }

    private Claims jwtParser(String token) {
        return Jwts.parser()
                .setSigningKey(jwtTokenProperties.getJwtSecret())
                .parseClaimsJws(token)
                .getBody();
    }
}