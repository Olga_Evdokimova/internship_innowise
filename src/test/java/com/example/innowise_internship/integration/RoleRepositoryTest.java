package com.example.innowise_internship.integration;

import com.example.innowise_internship.TestPostgresContainer;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.repository.RoleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@DirtiesContext
@TestPostgresContainer
public class RoleRepositoryTest {


    @Autowired
    private RoleRepository roleRepository;


    @Test
    void expectedTrueIfFoundRoleById() {
        Long roleId = 1L;
        Optional<Role> optionalRole = roleRepository.findById(roleId);
        assertTrue(optionalRole.isPresent());
    }
}
