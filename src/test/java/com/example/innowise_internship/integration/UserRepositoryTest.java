package com.example.innowise_internship.integration;

import com.example.innowise_internship.TestPostgresContainer;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@Sql(scripts = "/sql/insert-1.sql", executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = "/sql/delete-1.sql", executionPhase = AFTER_TEST_METHOD)
@DirtiesContext
@TestPostgresContainer
public class UserRepositoryTest {

    public static final Long USER_ID = 1L;
    public static final String EMAIL = "olgaaevdokim@gmail.com";

    @Autowired
    private UserRepository userRepository;

    @Test
    void expectedTrueIfFoundUserById() {
        Optional<User> optionalUser = userRepository.findById(USER_ID);
        assertTrue(optionalUser.isPresent());
    }

    @Test
    void expectedTrueIfFoundUserByLogin() {
        Optional<User> optionalUser = userRepository.findByEmail(EMAIL);
        assertTrue(optionalUser.isPresent());
    }

    @Test
    void findAllAdminsByUserId() {
        Optional<List<User>> admins = userRepository.findAllAById(USER_ID);
        assertTrue(admins.isPresent());
    }

    @Test
    void shouldSaveUser() {
        User user = User.builder()
                .withId(USER_ID)
                .withFirstName("string")
                .withLastName("string")
                .withEmail("string")
                .withPassword("string")
                .withLogin("login")
                .withUserRole(Role.builder().withId(1L).build())
                .withStartWorkHour(LocalDateTime.of(2021, 06, 12, 10, 00, 00))
                .withEndWorkHour(LocalDateTime.of(2021, 06, 12, 22, 00, 00))
                .build();
        User saveUser = userRepository.save(user);
        assertEquals(user, saveUser);
    }


}
