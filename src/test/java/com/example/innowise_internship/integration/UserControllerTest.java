package com.example.innowise_internship.integration;

import com.example.innowise_internship.InnowiseInternshipApplication;
import com.example.innowise_internship.TestPostgresContainer;
import com.example.innowise_internship.service.UserService;
import com.example.innowise_internship.util.FileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.client.servlet.OAuth2ClientAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.resource.servlet.OAuth2ResourceServerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;

@SpringBootTest(classes = InnowiseInternshipApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class, SecurityFilterAutoConfiguration.class, OAuth2ClientAutoConfiguration.class, OAuth2ResourceServerAutoConfiguration.class})
@AutoConfigureWebTestClient
@ActiveProfiles("test")
@TestPostgresContainer
public class UserControllerTest {

    @MockBean
    private UserService userService;
    @Autowired
    private WebTestClient webTestClient;


    @Test
    void shouldRollback() throws IOException {

        String dateTime = FileReaderUtil.readFromJsonFile("json/dateForRollback.json");
        webTestClient
                .post()
                .uri("/user/rollback")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(dateTime)
                .exchange()
                .expectStatus()
                .isOk();
    }
}
