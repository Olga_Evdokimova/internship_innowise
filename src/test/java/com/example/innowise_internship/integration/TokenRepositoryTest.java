package com.example.innowise_internship.integration;

import com.example.innowise_internship.TestPostgresContainer;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.repository.TokenRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@Sql(scripts = "/sql/insert-1.sql", executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = "/sql/delete-1.sql", executionPhase = AFTER_TEST_METHOD)
@DirtiesContext
@TestPostgresContainer
public class TokenRepositoryTest {

    public static final String REFRESH_TOKEN = "1";
    public static final String ACCESS_TOKEN = "1";
    public static final Role ROLE = Role.builder()
            .withId(1L)
            .withName("ROLE_ADMIN")
            .build();
    public static final User USER_EXPECTED = User.builder()
            .withId(2L)
            .withFirstName("string")
            .withLastName("string")
            .withLogin("string")
            .withPassword("string")
            .withEmail("string")
            .withUserRole(ROLE)
            .withEndWorkHour(LocalDateTime.of(2021, 06, 12, 22, 00, 00))
            .withStartWorkHour(LocalDateTime.of(2021, 06, 12, 10, 00, 00))
            .build();
    public static final Long DEFAULT_ID = 1L;
    public static final TokenDetails TOKEN_DETAILS_EXPECTED = TokenDetails.builder()
            .withId(1L)
            .withUser(USER_EXPECTED)
            .withRefreshToken(REFRESH_TOKEN)
            .withAccessToken(ACCESS_TOKEN)
            .build();
    @Autowired
    private TokenRepository tokenRepository;

    @Test
    void shouldReturnTokenDetails_WhenFindByRefreshToken() {
        Optional<TokenDetails> optionalTokenDetails = tokenRepository.findById(DEFAULT_ID);

        assertTrue(optionalTokenDetails.isPresent());
    }

    @Test
    void shouldReturnTokenDetailOnSave() {

        TokenDetails tokenDetails = tokenRepository.save(TOKEN_DETAILS_EXPECTED);
        assertEquals(TOKEN_DETAILS_EXPECTED, tokenDetails);
    }

    @Test
    void shouldReturnFalseWhenFoundDeletedTokenDetails() {
        tokenRepository.delete(TOKEN_DETAILS_EXPECTED);
        Optional<TokenDetails> byRefreshToken = tokenRepository.findById(DEFAULT_ID);
        assertFalse(byRefreshToken.isPresent());
    }
}
