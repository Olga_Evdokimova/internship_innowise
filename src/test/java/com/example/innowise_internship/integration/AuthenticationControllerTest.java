package com.example.innowise_internship.integration;

import com.example.innowise_internship.InnowiseInternshipApplication;
import com.example.innowise_internship.TestPostgresContainer;
import com.example.innowise_internship.config.security.reader.SecurityContextReader;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.entity.tokenStore.JwtTokenPair;
import com.example.innowise_internship.entity.tokenStore.TokenDetails;
import com.example.innowise_internship.provider.TokenHandler;
import com.example.innowise_internship.repository.RoleRepository;
import com.example.innowise_internship.repository.TokenRepository;
import com.example.innowise_internship.repository.UserRepository;
import com.example.innowise_internship.util.FileReaderUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.client.servlet.OAuth2ClientAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.resource.servlet.OAuth2ResourceServerAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityFilterAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.IOException;
import java.util.Optional;

import static com.example.innowise_internship.unit.service.AuthenticationServiceTest.*;
import static com.example.innowise_internship.unit.service.UserServiceTest.DEFAULT_PASSWORD;
import static com.example.innowise_internship.unit.service.UserServiceTest.DEFAULT_ROLE;
import static org.mockito.BDDMockito.given;

@SpringBootTest(classes = {InnowiseInternshipApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class, SecurityFilterAutoConfiguration.class, OAuth2ClientAutoConfiguration.class, OAuth2ResourceServerAutoConfiguration.class})
@AutoConfigureWebTestClient
@ActiveProfiles("test")
@TestPostgresContainer
public class AuthenticationControllerTest {

    public static final Long DEFAULT_ID = 20L;
    public static final Role ROLE = Role.builder().withId(2L).withName(DEFAULT_ROLE).build();

    public static final User DEFAULT_USER = User.builder()
            .withId(DEFAULT_ID)
            .withEmail(DEFAULT_EMAIL)
            .withPassword(DEFAULT_PASSWORD)
            .withUserRole(ROLE)
            .build();


    public static final TokenDetails TOKEN_DETAILS = TokenDetails.builder()
            .withUser(DEFAULT_USER)
            .withAccessToken(ACCESS_TOKEN)
            .withRefreshToken(REFRESH_TOKEN)
            .build();


    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private TokenHandler tokenHandler;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private TokenRepository tokenRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private SecurityContextReader securityContextReader;


    @Test
    void shouldReturnJwtResponseOnLogin() throws IOException {
        TokenDetails tokenDetails = TokenDetails.builder()
                .withUser(DEFAULT_USER)
                .withAccessToken(ACCESS_TOKEN)
                .withRefreshToken(REFRESH_TOKEN)
                .build();

        String jwtResponse = FileReaderUtil.readFromJsonFile("json/jwtResponse.json");
        String loginRequest = FileReaderUtil.readFromJsonFile("json/loginRequest.json");

        given(userRepository.findByEmail(DEFAULT_EMAIL)).willReturn(Optional.ofNullable(DEFAULT_USER));
        given(tokenHandler.generateTokenDetails(DEFAULT_USER)).willReturn(tokenDetails);


        webTestClient
                .post()
                .uri("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(loginRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .json(jwtResponse);
    }

    @Test
    void shouldRefreshToken() throws IOException {
        JwtTokenPair jwtTokenPair = JwtTokenPair.builder()
                .withAccessToken(ACCESS_TOKEN)
                .withRefreshToken(REFRESH_TOKEN)
                .build();

        SecurityContext context = getSecurityContext();

        given(tokenRepository.findByRefreshToken(REFRESH_TOKEN)).willReturn(Optional.ofNullable(TOKEN_DETAILS));
        given(tokenHandler.generateTokenPair(DEFAULT_USER)).willReturn(jwtTokenPair);
        given(securityContextReader.getAuthenticationFromSecurityContext()).willReturn(context.getAuthentication());
        given(tokenRepository.save(TOKEN_DETAILS)).willReturn(TOKEN_DETAILS);

        String jwtResponse = FileReaderUtil.readFromJsonFile("json/jwtResponse.json");
        String refreshRequest = FileReaderUtil.readFromJsonFile("json/refresh.json");

        webTestClient
                .put()
                .uri("/auth/refreshtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(refreshRequest)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .json(jwtResponse);

    }

    @Test
    void shouldRegisterUser() throws IOException {
        Long roleId = 2L;
        given(roleRepository.findById(roleId)).willReturn(Optional.ofNullable(ROLE));
        given(userRepository.save(DEFAULT_USER)).willReturn(DEFAULT_USER);

        String userRegisterDto = FileReaderUtil.readFromJsonFile("json/userRegisterDto.json");
        String userDto = FileReaderUtil.readFromJsonFile("json/userDto.json");

        webTestClient
                .post()
                .uri("/auth/user/signUp")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(userRegisterDto)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .json(userDto);
    }

    @Test
    void shouldLogout() {
        given(tokenRepository.findByRefreshToken(REFRESH_TOKEN)).willReturn(Optional.ofNullable(TOKEN_DETAILS));

        SecurityContext context = getSecurityContext();
        given(securityContextReader.getAuthenticationFromSecurityContext()).willReturn(context.getAuthentication());

        webTestClient
                .delete()
                .uri("/auth/logout?refreshToken=" + REFRESH_TOKEN)
                .exchange()
                .expectStatus()
                .isOk();

    }

    @Test
    void shouldGetTokenNotFoundException_WhenLogout_AndNotFoundById() {
        given(tokenRepository.findByRefreshToken(REFRESH_TOKEN)).willReturn(Optional.ofNullable(TOKEN_DETAILS));

        SecurityContext context = getSecurityContext();
        given(securityContextReader.getAuthenticationFromSecurityContext()).willReturn(context.getAuthentication());

        webTestClient
                .delete()
                .uri("/auth/logout?refreshToken=" + "")
                .exchange()
                .expectStatus()
                .isNotFound();

    }


    private SecurityContext getSecurityContext() {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        Role userRole = Role.builder().withName(DEFAULT_USER.getUserRole().getName()).build();
        User principal = User.builder()
                .withLogin(DEFAULT_USER.getLogin())
                .withUserRole(userRole)
                .build();
        Authentication auth =
                new UsernamePasswordAuthenticationToken(principal, "password", principal.getAuthorities());
        context.setAuthentication(auth);
        return context;
    }
}
