package com.example.innowise_internship.integration;

import com.example.innowise_internship.TestPostgresContainer;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ActiveProfiles("test")
@Sql(scripts = "/sql/dataForRollback.sql", executionPhase = BEFORE_TEST_METHOD)
@DirtiesContext
@TestPostgresContainer
public class UserRepositoryRollbackMethodTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    void shouldNotPresent_WhenInsertTwoUsers_AndThenRollback() {
        LocalDateTime date = LocalDateTime.of(2021, 06, 12, 10, 00, 00);
        userRepository.rollback(date);
        Optional<User> user = userRepository.findById(2L);
        assertFalse(user.isPresent());
    }
}
