package com.example.innowise_internship;

import com.example.innowise_internship.initializer.Postgres;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class TestPostgresClass implements BeforeAllCallback, AfterAllCallback {


    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception {
        Postgres.container.close();
    }

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        Postgres.container.start();
    }
}
