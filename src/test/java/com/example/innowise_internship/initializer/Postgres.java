package com.example.innowise_internship.initializer;

import com.example.innowise_internship.util.PropertiesUtil;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.api.model.Ports;
import org.testcontainers.containers.PostgreSQLContainer;


public class Postgres {
    public static final PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:latest")
            .withUsername(PropertiesUtil.getProperty("spring.datasource.username"))
            .withPassword(PropertiesUtil.getProperty("spring.datasource.password"))
            .withCreateContainerCmdModifier(cmd -> cmd.getHostConfig()
                    .withPortBindings(new PortBinding(Ports.Binding.bindPort(5437), ExposedPort.tcp(5432))));

}
