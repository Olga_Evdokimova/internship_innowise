package com.example.innowise_internship.security;

import com.example.innowise_internship.InnowiseInternshipApplication;
import com.example.innowise_internship.dto.JwtResponse;
import com.example.innowise_internship.dto.LoginRequest;
import com.example.innowise_internship.dto.TokenRefreshRequest;
import com.example.innowise_internship.dto.role.RoleDto;
import com.example.innowise_internship.dto.user.UserRegisterDto;
import com.example.innowise_internship.entity.Role;
import com.example.innowise_internship.entity.User;
import com.example.innowise_internship.service.AuthenticationService;
import com.example.innowise_internship.util.FileReaderUtil;
import com.example.innowise_internship.validator.JwtValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.reactive.server.WebTestClient;

import static com.example.innowise_internship.unit.service.AuthenticationServiceTest.DEFAULT_EMAIL;
import static org.mockito.BDDMockito.given;


@SpringBootTest(classes = InnowiseInternshipApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
@TestPropertySource(locations = {"classpath:application-test-values.properties"})
@DirtiesContext
public class AuthenticationControllerTest {

    public static final String REFRESH_TOKEN = "refreshToken";

    public static final User USER = User.builder()
            .withLogin("login")
            .withPassword("password")
            .withUserRole(Role.builder().withName("ROLE_USER").build())
            .withId(20L)
            .build();
    public static final JwtResponse JWT_RESPONSE_OBJECT = JwtResponse.builder()
            .withUserId(20L)
            .withAccessToken("accessToken")
            .withRefreshToken("refreshToken")
            .build();
    public static final TokenRefreshRequest TOKEN_REFRESH_REQUEST = TokenRefreshRequest.builder()
            .withRefreshToken("refreshToken")
            .build();
    @Value("${jwt}")
    public String token;
    @Autowired
    private WebTestClient webTestClient;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private JwtValidator jwtValidator;
    @MockBean
    private UserDetailsService userDetailsService;

    @Test
    void shouldReturnJwtResponseOnLogin() throws Exception {

        String jwtResponse = FileReaderUtil.readFromJsonFile("json/jwtResponse.json");
        String loginRequest = FileReaderUtil.readFromJsonFile("json/loginRequest.json");

        LoginRequest loginRequestObject = LoginRequest.builder()
                .withLogin("email")
                .withPassword("password")
                .build();

        given(authenticationService.login(loginRequestObject)).willReturn(JWT_RESPONSE_OBJECT);

        webTestClient
                .post()
                .uri("/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(loginRequest)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .json(jwtResponse);

    }

    @Test
    void shouldReturnJwtResponseOnRefresh() throws Exception {

        String refresh = FileReaderUtil.readFromJsonFile("json/refresh.json");
        String jwtResponse = FileReaderUtil.readFromJsonFile("json/jwtResponse.json");

        given(jwtValidator.validateJwtToken(token)).willReturn(true);
        given(jwtValidator.getUserNameFromJwtToken(token)).willReturn("login");
        given(userDetailsService.loadUserByUsername("login")).willReturn(USER);
        given(authenticationService.refresh(TOKEN_REFRESH_REQUEST)).willReturn(JWT_RESPONSE_OBJECT);

        webTestClient
                .put()
                .uri("/auth/refreshtoken")
                .headers(http -> http.setBearerAuth(token))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(refresh)
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody()
                .json(jwtResponse);

    }

    @Test
    void shouldReturnStatusForbiddenOnRefresh_WhenNotAttachToken() throws Exception {

        String refresh = FileReaderUtil.readFromJsonFile("json/refresh.json");
        TokenRefreshRequest request = TokenRefreshRequest.builder()
                .withRefreshToken("refreshToken")
                .build();

        given(userDetailsService.loadUserByUsername("login")).willReturn(USER);
        given(authenticationService.refresh(request)).willReturn(JWT_RESPONSE_OBJECT);

        webTestClient
                .put()
                .uri("/auth/refreshtoken")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(refresh)
                .exchange()
                .expectStatus()
                .isForbidden();
    }

    @Test
    void shouldLogout() {
        given(jwtValidator.validateJwtToken(token)).willReturn(true);
        given(jwtValidator.getUserNameFromJwtToken(token)).willReturn("login");
        given(userDetailsService.loadUserByUsername("login")).willReturn(USER);

        webTestClient
                .delete()
                .uri("/auth/logout?refreshToken=" + REFRESH_TOKEN)
                .headers(http -> http.setBearerAuth(token))
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk();

    }

    @Test
    void shouldRegisterUser_WhenHasRoleAdmin() {
        UserRegisterDto userRegisterDto = UserRegisterDto.builder()
                .withLogin(DEFAULT_EMAIL)
                .withRoleDto(RoleDto.builder().withName("ROLE_USER").build())
                .build();

        User user = User.builder()
                .withLogin("login")
                .withPassword("password")
                .withUserRole(Role.builder().withName("ROLE_ADMIN").build())
                .withId(20L)
                .build();
        given(jwtValidator.validateJwtToken(token)).willReturn(true);
        given(jwtValidator.getUserNameFromJwtToken(token)).willReturn("login");
        given(userDetailsService.loadUserByUsername("login")).willReturn(user);

        webTestClient
                .post()
                .uri("auth/user/signUp")
                .headers(http -> http.setBearerAuth(token))
                .bodyValue(userRegisterDto)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isOk();
    }

    @Test
    void shouldNotRegisterUser_WhenHasRoleUser() {
        UserRegisterDto userRegisterDto = UserRegisterDto.builder()
                .withLogin("bob")
                .withRoleDto(RoleDto.builder().withName("ROLE_USER").build())
                .build();

        User user = User.builder()
                .withLogin("login")
                .withPassword("password")
                .withUserRole(Role.builder().withId(2L).withName("ROLE_USER").build())
                .withId(20L)
                .build();
        given(jwtValidator.validateJwtToken(token)).willReturn(true);
        given(jwtValidator.getUserNameFromJwtToken(token)).willReturn("login");
        given(userDetailsService.loadUserByUsername("login")).willReturn(user);

        webTestClient
                .post()
                .uri("auth/user/signUp")
                .headers(http -> http.setBearerAuth("token"))
                .bodyValue(userRegisterDto)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus()
                .isForbidden();
    }

}
