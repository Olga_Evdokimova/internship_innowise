package com.example.innowise_internship.security.oauth2;

import com.example.innowise_internship.InnowiseInternshipApplication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockOAuth2Client;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.mockOidcLogin;

@SpringBootTest(classes = InnowiseInternshipApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class OAuth2Tests {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    void shouldLoginWithOAuth2() {
        String clientId = "468058151362-uo2g5qbuqr3olo23qo93d2lnb7s7hgka.apps.googleusercontent.com";
        String clientSecret = "OhlShDOUd-UBcsNqnWY7zTR7";

        webTestClient
                .mutateWith(mockOAuth2Client("google"))
                .post()
                .uri("/oauth/token?grant_type=client_credentials" +
                        "&client_id=" + clientId +
                        "&client_secret=" + clientSecret +
                        "&username=olgaaevdokim@gmail.com" +
                        "&password=Riomy778v")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.access_token").isNotEmpty()
                .jsonPath("$.refresh_token").isNotEmpty();
    }
}
